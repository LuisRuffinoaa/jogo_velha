// const. é um dado e ticTacToe um obj. 
const tic_tac_toe = {
    // array é um espaço 
    board: ["0","1","2","3","4","5","6","7","8"],
      
    simbols:{
        options: ['O','X'],

        turn_index: 0,

        change: function(){
            this.turn_index = (this.turn_index === 0 ? 1:0);
        }
    },


      // container é um espaço que se iniciara nulo
container_element: null,

gameover: false,

winning_sequences: [
[0,1,2],
[3,4,5],
[6,7,8],
[0,3,6],
[1,4,7],
[2,5,8],
[0,4,8],
[2,4,6],
],

// esta associando o container da interface com o obj. container
init: function(container){
    this.container_element = container;
},

make_play: function(position){

// verifica se o jogo acabou
if(this.gameover) return false;

if (this.board[position] === '') {

this.board[position] = this.simbols.options [this.simbols.turn_index];
this.draw();

let winning_sequences_index = this.check_winning_sequences( this.simbols.options[this.simbols.turn_index]);


if (winning_sequences_index >= 0){
this.game_is_over();
} else {

    this.simbols.change();
};
 return true;


}else {
return false;
}

},

game_is_over: function() {
this.gameover = true;
console.log('Game Over');
},


start: function(){
this.board.fill('');
this.draw();
this.gameover = false;

},

check_winning_sequences: function(simbol){

    for (i in this.winning_sequences){

if(this.board [ this.winning_sequences[i][0] ] == simbol &&
    this.board [ this.winning_sequences[i][1] ] == simbol &&
    this.board [ this.winning_sequences[i][2] ] == simbol){

Console.log('winning sequences INDEX: ' + i);
   return i;
    }
  };
},


// draw escreve a funcao na board
draw: function(){
let content = "";
// for é uma estrutura de repetiçao(loop)
for (i in this.board){
//  content recebe a adiçao das "divs"  
// divs estao como strings
    // content += "<div>"  + this.board[i] + "</div>";

    content += '<div onclick="tic_tac_toe.make_play(' + i + ')" >' + this.board[i] + '</div>';
}

this.container_element.innerHTML = content;

},

};

